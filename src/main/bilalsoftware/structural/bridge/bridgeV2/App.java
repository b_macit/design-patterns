package main.bilalsoftware.structural.bridge.bridgeV2;

import main.bilalsoftware.structural.bridge.bridgeV2.cihaz.BilgisayarKulaklikVeFizy;
import main.bilalsoftware.structural.bridge.bridgeV2.cihaz.Telefon;
import main.bilalsoftware.structural.bridge.bridgeV2.muzikcalar.Spotify;
import main.bilalsoftware.structural.bridge.bridgeV2.sescihazi.Hoparlor;

public class App {

    public static void main(String[] args) {
        Muzik muzik = new Muzik("Sezen Aksu- Gülümse", " Gülümse Hadi gülümse ");

        BilgisayarKulaklikVeFizy kulaklikVeFizy = new BilgisayarKulaklikVeFizy();
        kulaklikVeFizy.muzikCal(muzik);

        Telefon telefon = new Telefon(new Hoparlor(), new Spotify());
        telefon.muzikCal(muzik);
    }
}
