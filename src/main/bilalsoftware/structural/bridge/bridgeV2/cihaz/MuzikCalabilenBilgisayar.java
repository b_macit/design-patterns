package main.bilalsoftware.structural.bridge.bridgeV2.cihaz;

import main.bilalsoftware.structural.bridge.bridgeV2.Muzik;
import main.bilalsoftware.structural.bridge.bridgeV2.muzikcalar.MuzikCalar;
import main.bilalsoftware.structural.bridge.bridgeV2.sescihazi.SesCihazi;

public abstract class MuzikCalabilenBilgisayar {

    protected SesCihazi sesCihazi;
    protected MuzikCalar muzikCalar;

    public MuzikCalabilenBilgisayar() {
    }

    public MuzikCalabilenBilgisayar(SesCihazi sesCihazi, MuzikCalar muzikCalar) {
        this.sesCihazi = sesCihazi;
        this.muzikCalar = muzikCalar;
    }

    void muzikCal(Muzik muzik) {
        final String ses = muzikCalar.muzikCal(muzik);
        sesCihazi.sesiCal(ses);
    }
}
