package main.bilalsoftware.structural.bridge.bridgeV2.cihaz;

import main.bilalsoftware.structural.bridge.bridgeV2.Muzik;
import main.bilalsoftware.structural.bridge.bridgeV2.muzikcalar.Fizy;
import main.bilalsoftware.structural.bridge.bridgeV2.sescihazi.Kulaklik;

public class BilgisayarKulaklikVeFizy extends MuzikCalabilenBilgisayar {
    public BilgisayarKulaklikVeFizy() {
        sesCihazi = new Kulaklik();
        muzikCalar = new Fizy();
    }

    @Override
    public void muzikCal(Muzik muzik) {
        System.out.println("Bilgisayar Calisti");
        super.muzikCal(muzik);
    }
}
