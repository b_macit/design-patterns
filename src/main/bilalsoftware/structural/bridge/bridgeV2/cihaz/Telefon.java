package main.bilalsoftware.structural.bridge.bridgeV2.cihaz;

import main.bilalsoftware.structural.bridge.bridgeV2.Muzik;
import main.bilalsoftware.structural.bridge.bridgeV2.muzikcalar.MuzikCalar;
import main.bilalsoftware.structural.bridge.bridgeV2.sescihazi.SesCihazi;

public class Telefon extends MuzikCalabilenBilgisayar {

    public Telefon(SesCihazi sesCihazi, MuzikCalar muzikCalar) {
        super(sesCihazi, muzikCalar);
    }

    @Override
    public void muzikCal(Muzik muzik) {
        System.out.println("Telefon Calisti");
        super.muzikCal(muzik);
    }
}
