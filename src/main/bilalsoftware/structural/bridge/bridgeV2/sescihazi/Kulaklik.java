package main.bilalsoftware.structural.bridge.bridgeV2.sescihazi;

public class Kulaklik implements SesCihazi{
    @Override
    public void sesiCal(String ses) {
        System.out.println("Kulaklik ses veriyor " + ses);
    }
}
