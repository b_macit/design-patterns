package main.bilalsoftware.structural.bridge.bridgeV2.muzikcalar;

import main.bilalsoftware.structural.bridge.bridgeV2.Muzik;

public class Fizy implements MuzikCalar{
    @Override
    public String muzikCal(Muzik muzik) {
        System.out.println("Fizy " + muzik + " sarkısını çalışyır");
        return muzik.getSes();
    }
}
