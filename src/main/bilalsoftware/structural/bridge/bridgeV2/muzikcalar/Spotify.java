package main.bilalsoftware.structural.bridge.bridgeV2.muzikcalar;

import main.bilalsoftware.structural.bridge.bridgeV2.Muzik;

public class Spotify implements MuzikCalar{

    public String muzikCal(Muzik muzik) {
        System.out.println("Spotify " + muzik + " sarkısını çalışyır");
        return muzik.getSes();
    }
}
