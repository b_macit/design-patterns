package main.bilalsoftware.structural.bridge.bridgeV2.muzikcalar;

import main.bilalsoftware.structural.bridge.bridgeV2.Muzik;

public interface MuzikCalar {

    String muzikCal(Muzik muzik);
}
