package main.bilalsoftware.structural.bridge.bridgeV1;

public class App {

    public static void main(String[] args) {
        Muzik muzik = new Muzik("Sezen Aksu- Gülümse", " Gülümse Hadi gülümse ");

        Bilgisayar bilgisayar = new Bilgisayar();

        bilgisayar.muzikCal(muzik);
    }
}
