package main.bilalsoftware.structural.bridge.bridgeV1;

public class Spotify {

    public String muzikCal(Muzik muzik) {
        System.out.println("Spotify " + muzik + " sarkısını çalışyır");
        return muzik.getSes();
    }
}
