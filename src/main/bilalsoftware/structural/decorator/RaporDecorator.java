package main.bilalsoftware.structural.decorator;

public abstract class RaporDecorator implements Rapor{

    private Rapor rapor;

    protected RaporDecorator(Rapor rapor) {
        this.rapor = rapor;
    }

    @Override
    public String getMetin() {
        return rapor.getMetin();
    }
}
