package main.bilalsoftware.structural.decorator;

import java.util.ArrayList;
import java.util.List;

public class RaporUtil {

    public static String getCizgiliMetin(String metin) {
        final String[] satirList = metin.split("\n");
        String cizligiMetin = "";
        for(String satir : satirList) {
            String cizgiliSatir = herSatiraCizgiEkle(satir);
            cizligiMetin = cizligiMetin + cizgiliSatir;
        }
        return cizligiMetin;
    }

    private static String herSatiraCizgiEkle(String satir) {
        String yeniSatir;
        if(satir.trim().length() == 0 ) {
            yeniSatir = "\n";
        }else {
            yeniSatir = "\n- " + satir;
        }
        return yeniSatir;
    }

    public static String metniSigdir(String metin, int genislik) {
        String kucukMetin = "";
        final String[] satirList = metin.split("\n");
        for(String satir : satirList) {

            if(satir.length() <= genislik) {
                kucukMetin = ekleVeYeniSatirEkle(kucukMetin, satir);
            }else {
                List<String> parcalanmisList = parcala(satir, genislik);

                for(String yeniSatir : parcalanmisList){
                    kucukMetin = ekleVeYeniSatirEkle(kucukMetin, yeniSatir);
                }
            }
        }
        return kucukMetin;
    }

    private static String ekleVeYeniSatirEkle(String anaMetin, String eklenecekMetin) {
        return anaMetin + eklenecekMetin + "\n";
    }

    /**
     * genislik = 30
     *
     * satir:
     *  1-> 45 (30,15)
     *  2-> 25 (25)
     *  3-> 65 (30, 30, 5)
     * @param satir
     * @param genislik
     * @return
     */
    private static List<String> parcala(String satir, int genislik) {
        List<String> parcaliSatirListesi= new ArrayList<>();
        for(int i = 0 ; i <= satir.length() / 30 ; i++){
            final int ilkHane = genislik * i;
            int sonHane = ilkHane + genislik;

            if(sonHane > satir.length()) {
                sonHane = satir.length();
            }
            final String parcaliSatir = satir.substring(ilkHane, sonHane);

            if(parcaliSatir.trim().length() > 0) {
                parcaliSatirListesi.add(parcaliSatir);
            }
        }
        return parcaliSatirListesi;
    }

    public static String sonaImzaEkle(String metin) {

        String imza = "\n\n" + "\t\t\t\t\t\t İmza : ";

        return metin + imza;
    }
}
