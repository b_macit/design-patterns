package main.bilalsoftware.structural.decorator;

public class CizgiliRaporDecorator extends RaporDecorator{

    protected CizgiliRaporDecorator(Rapor rapor) {
        super(rapor);
    }

    @Override
    public String getMetin() {
        final String metin = super.getMetin();
        String cizgiliMetin = RaporUtil.getCizgiliMetin(metin);
        return cizgiliMetin;
    }
}
