package main.bilalsoftware.structural.decorator;

public class App {

    public static void main(String[] args) {
        String icerik = getRaporIcerik();

        //final String metin = duzRaporCiktisi(icerik);
        //final String metin = cizgiliRaporCiktisi(icerik);
        //final String metin = imzaliRaporCiktisi(icerik);
        final DuzRapor duzRapor = new DuzRapor(icerik);
        CizgiliRaporDecorator cizgiliRaporDecorator = new CizgiliRaporDecorator(duzRapor);
        KucukRaporDecorator kucukRaporDecorator = new KucukRaporDecorator(cizgiliRaporDecorator);

        final String metin = kucukRaporDecorator.getMetin();


        System.out.println(metin);
    }

    private static String imzaliRaporCiktisi(String icerik) {
        final DuzRapor duzRapor = new DuzRapor(icerik);
        ImzaliRaporDecorator imzaliRaporDecorator = new ImzaliRaporDecorator(duzRapor);
        return imzaliRaporDecorator.getMetin();
    }

    private static String cizgiliRaporCiktisi(String icerik) {
        final DuzRapor duzRapor = new DuzRapor(icerik);
        CizgiliRaporDecorator cizgiliRaporDecorator = new CizgiliRaporDecorator(duzRapor);
        return cizgiliRaporDecorator.getMetin();
    }

    private static String duzRaporCiktisi(String icerik) {
        final DuzRapor duzRapor = new DuzRapor(icerik);

        return duzRapor.getMetin();
    }

    private static String getRaporIcerik() {
        return  "Lorem Ipsum is simply dummy text of the printing and typesetting industry.\n" +
                "\n Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, " +
                "\nwhen an unknown printer took a galley of type and scrambled it to make a type specimen book. " +
                "\nIt has survived not only five centuries, but also the leap into electronic typesetting, " +
                "\nremaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, " +
                "\nand more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
    }
}
