package main.bilalsoftware.structural.decorator;

public class KucukRaporDecorator extends RaporDecorator{

    private int genislik;

    protected KucukRaporDecorator(Rapor rapor) {
        super(rapor);
        this.genislik = 30;
    }

    @Override
    public String getMetin() {
        final String metin = super.getMetin();
        String kucukMetin = RaporUtil.metniSigdir(metin, genislik);
        return kucukMetin;
    }
}
