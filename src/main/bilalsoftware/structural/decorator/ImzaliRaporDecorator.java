package main.bilalsoftware.structural.decorator;

public class ImzaliRaporDecorator extends RaporDecorator {

    protected ImzaliRaporDecorator(Rapor rapor) {
        super(rapor);
    }

    @Override
    public String getMetin() {
        final String metin = super.getMetin();
        String imzaliMetin = RaporUtil.sonaImzaEkle(metin);
        return imzaliMetin;
    }
}
