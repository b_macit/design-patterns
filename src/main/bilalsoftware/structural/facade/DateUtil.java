package main.bilalsoftware.structural.facade;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    public static Date getGununTarihi() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

        final String dateString = sdf.format(new Date());
        Date date = null;
        try {
            date = sdf.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

    public static long gunFarkiniHesapla(Date ilkTarih, Date sonTarih) {
        long gunKatsayisi = 24 * 60 * 60 * 1000;

        final long ilk = ilkTarih.getTime();
        final long son = sonTarih.getTime();

        final long gunFarki = (son / gunKatsayisi) - (ilk / gunKatsayisi);

        return gunFarki;
    }

    public static Long tarihinYili(Date date) {
        final String stringYil = new SimpleDateFormat("yyyy").format(date);

        return Long.valueOf(stringYil);
    }

    public static Long tarihinAyi(Date date) {
        final String stringAy = new SimpleDateFormat("MM").format(date);

        return Long.valueOf(stringAy);
    }

    public static Date tarihOlustur(String s) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        try {
            final Date date = sdf.parse(s);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
