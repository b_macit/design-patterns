package main.bilalsoftware.structural.facade.hukuksal;

import java.util.List;

public class HukuksalKontrolService {

    private HukuksalSurecEntityService hukuksalSurecEntityService;

    public HukuksalKontrolService() {
        this.hukuksalSurecEntityService = new HukuksalSurecEntityService();
    }

    public Boolean isHukuksalIslemde(Long borcId) {
        List<Long> hukuksalIslemdekiBorcList = hukuksalSurecEntityService.findAllHukuksalIslem();

        final boolean islemHukuksalIslemde = hukuksalIslemdekiBorcList.contains(borcId);

        if(islemHukuksalIslemde) {
            System.out.println("Borç hukuksal işlemde, Tahsil edilemez!!");
        }
        return islemHukuksalIslemde;
    }
}
