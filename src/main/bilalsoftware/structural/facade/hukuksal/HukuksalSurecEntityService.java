package main.bilalsoftware.structural.facade.hukuksal;

import java.util.Arrays;
import java.util.List;

public class HukuksalSurecEntityService {

    public List<Long> findAllHukuksalIslem() {
        final List<Long> longs = Arrays.asList(1L, 2L, 3L, 4L);
        return longs;
    }
}
