package main.bilalsoftware.structural.facade.tahsilat.kredikarti;

import main.bilalsoftware.structural.facade.DateUtil;
import main.bilalsoftware.structural.facade.borc.OdemeBilgileriDto;

import java.util.Date;

public class KrediKartiKontrolService {

    public boolean isKrediKartiGecerli(OdemeBilgileriDto odemeBilgileriDto) {
        final Long sonKullanmaAy = odemeBilgileriDto.getSonKullanmaAy();
        final Long sonKullanmaYil = odemeBilgileriDto.getSonKullanmaYil();

        final Date gununTarihi = DateUtil.getGununTarihi();

        final Long gununAyi = DateUtil.tarihinAyi(gununTarihi);
        final Long gununYili = DateUtil.tarihinYili(gununTarihi);

        boolean isKrediKartiGecerli = false;
        if(sonKullanmaYil.compareTo(gununYili) > 0) {
            isKrediKartiGecerli = true;
        } else if (sonKullanmaYil.compareTo(gununYili) == 0) {
            if(sonKullanmaAy.compareTo(gununAyi) >= 0) {
                isKrediKartiGecerli = true;
            }
        }

        if(!isKrediKartiGecerli) {
            System.out.println("Gecersiz Kredi Karti!!");
        }

        return isKrediKartiGecerli;
    }
}
