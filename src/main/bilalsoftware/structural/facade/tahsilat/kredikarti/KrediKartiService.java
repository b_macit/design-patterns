package main.bilalsoftware.structural.facade.tahsilat.kredikarti;

import main.bilalsoftware.structural.facade.borc.OdemeBilgileriDto;

import java.math.BigDecimal;

public class KrediKartiService {

    private KrediKartiKontrolService krediKartiKontrolService;

    public KrediKartiService() {
        this.krediKartiKontrolService = new KrediKartiKontrolService();
    }

    public boolean krediKartindanTahsilDene(OdemeBilgileriDto odemeBilgileriDto, BigDecimal odenecekTutar) {
        final boolean krediKartiGecerli = krediKartiKontrolService.isKrediKartiGecerli(odemeBilgileriDto);

        if(!krediKartiGecerli) {
            return false;
        }

        boolean isLimitVar = isYeterliLimitVar(odemeBilgileriDto, odenecekTutar);

        if(!isLimitVar) {
            return false;
        }

        System.out.println("Kredi kartından " + odenecekTutar + " TL tahsil edilmiştir.");

        return true;
    }

    private boolean isYeterliLimitVar(OdemeBilgileriDto odemeBilgileriDto, BigDecimal odenecekTutar) {

        BigDecimal limit = getKrediKartiLimiti(odemeBilgileriDto);
        final boolean isYeterliLimitVar = limit.compareTo(odenecekTutar) > 0;
        if(isYeterliLimitVar) {
            System.out.println("Kartta yeterli limit bulunmaktadır.");
        }

        return isYeterliLimitVar;
    }

    private BigDecimal getKrediKartiLimiti(OdemeBilgileriDto odemeBilgileriDto) {
        BigDecimal limit = BigDecimal.ZERO;
        final String kartUzerindekiIsim = odemeBilgileriDto.getKartUzerindekiIsim();

        if(kartUzerindekiIsim.contains("Bilal")) {
            limit =  new BigDecimal(100);
        } else if (kartUzerindekiIsim.contains("Ayse")) {
            limit = new BigDecimal(1000);
        } else if (kartUzerindekiIsim.contains("Yusuf")) {
            limit = BigDecimal.TEN;
        }

        return limit;
    }
}
