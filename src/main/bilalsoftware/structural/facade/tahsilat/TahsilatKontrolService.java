package main.bilalsoftware.structural.facade.tahsilat;

import main.bilalsoftware.structural.facade.borc.Borc;
import main.bilalsoftware.structural.facade.borc.BorcTuru;
import main.bilalsoftware.structural.facade.hukuksal.HukuksalKontrolService;
import main.bilalsoftware.structural.facade.yapilandirma.YapilandirmaKontrolService;

public class TahsilatKontrolService {

    private YapilandirmaKontrolService yapilandirmaKontrolService;
    private HukuksalKontrolService hukuksalKontrolService;

    public TahsilatKontrolService() {
        this.yapilandirmaKontrolService = new YapilandirmaKontrolService();
        this.hukuksalKontrolService = new HukuksalKontrolService();
    }

    public boolean isTahislataUygun(Borc borc) {

        boolean yapilandirmaBozulmali = false;
        if(BorcTuru.YAPILANDIRMA.equals(borc.getBorcTuru())) {
            yapilandirmaBozulmali = yapilandirmaKontrolService.isYapilandirmaBozulmali(borc.getVadeTarihi());
        }

        boolean hukuksalIslemde = hukuksalKontrolService.isHukuksalIslemde(borc.getId());

        final boolean isBorcTahsilataUygunMu = !(yapilandirmaBozulmali || hukuksalIslemde);

        return isBorcTahsilataUygunMu;
    }
}
