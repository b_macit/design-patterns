package main.bilalsoftware.structural.facade.yapilandirma;

import main.bilalsoftware.structural.facade.DateUtil;

import java.util.Date;

public class YapilandirmaKontrolService {

    public boolean isYapilandirmaBozulmali(Date vadeTarihi) {

        final Date gununTarihi = DateUtil.getGununTarihi();

        final long gunFarki = DateUtil.gunFarkiniHesapla(vadeTarihi, gununTarihi);

        final boolean isYapilandirmaBozuldu = gunFarki > 90;

        if(isYapilandirmaBozuldu) {
            System.out.println("Borcun yapılandırması bozulmalı, Tahsil edilemez!!");
        }

        return isYapilandirmaBozuldu;
    }
}
