package main.bilalsoftware.structural.facade;

import main.bilalsoftware.structural.facade.borc.Borc;
import main.bilalsoftware.structural.facade.borc.OdemeBilgileriDto;
import main.bilalsoftware.structural.facade.gecikme.GecikmeZammiHesaplamaService;
import main.bilalsoftware.structural.facade.tahsilat.TahsilatKontrolService;
import main.bilalsoftware.structural.facade.tahsilat.kredikarti.KrediKartiService;

import java.math.BigDecimal;

public class TahsilatFacade {

    private GecikmeZammiHesaplamaService gecikmeZammiHesaplamaService;
    private TahsilatKontrolService tahsilatKontrolService;
    private KrediKartiService krediKartiService;

    public TahsilatFacade() {
        this.gecikmeZammiHesaplamaService = new GecikmeZammiHesaplamaService();
        this.tahsilatKontrolService = new TahsilatKontrolService();
        this.krediKartiService = new KrediKartiService();
    }

    public boolean tahsilatYap(Borc borc, OdemeBilgileriDto odemeBilgileriDto) {

        boolean tahsilataUygun = tahsilatKontrolService.isTahislataUygun(borc);

        if(!tahsilataUygun) {
            return false;
        }

        final BigDecimal borcTutari = borc.getBorcTutari();
        final BigDecimal getcikmeZammi = gecikmeZammiHesaplamaService.gecikmeZammiHesapla(borc.getBorcTutari(), borc.getVadeTarihi());

        final BigDecimal odenecekTutar = borcTutari.add(getcikmeZammi);

        System.out.println("Bilgi: Kredi kartından " + odenecekTutar + " TL tahsilat deneniyor.");

        final boolean isSuccess = krediKartiService.krediKartindanTahsilDene(odemeBilgileriDto, odenecekTutar);
        return isSuccess;
    }
}
