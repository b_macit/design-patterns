package main.bilalsoftware.structural.facade;

import main.bilalsoftware.structural.facade.borc.Borc;
import main.bilalsoftware.structural.facade.borc.BorcTuru;
import main.bilalsoftware.structural.facade.borc.OdemeBilgileriDto;

import java.math.BigDecimal;
import java.util.Date;

public class App {

    public static void main(String[] args) {
        TahsilatFacade tahsilatFacade = new TahsilatFacade();

        Date vadeTarihi = DateUtil.tarihOlustur("01.01.2020");
        Borc borc = new Borc(1L, BorcTuru.NORMAL, new BigDecimal(100), vadeTarihi);

        OdemeBilgileriDto odemeBilgileriDto = new OdemeBilgileriDto("Bilal Macit", "123123123", 01L, 2020L, 153L);

        final boolean isSuccess = tahsilatFacade.tahsilatYap(borc, odemeBilgileriDto);

        if(isSuccess) {
            System.out.println("Tahsilat Yapıldı.");
        } else {
            System.out.println("Tahsilay yapılamadı.");
        }
    }
}
