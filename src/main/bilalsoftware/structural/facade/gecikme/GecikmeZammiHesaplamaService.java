package main.bilalsoftware.structural.facade.gecikme;

import main.bilalsoftware.structural.facade.DateUtil;

import java.math.BigDecimal;
import java.util.Date;

public class GecikmeZammiHesaplamaService {

    private GecikmeZammiOraniEntityServis gecikmeZammiOraniEntityServis;

    public GecikmeZammiHesaplamaService() {
        this.gecikmeZammiOraniEntityServis = new GecikmeZammiOraniEntityServis();
    }

    public BigDecimal gecikmeZammiHesapla(BigDecimal tutar, Date vadeTarihi) {
        BigDecimal gecikmeZammi = BigDecimal.ZERO;

        if(vadeTarihi.compareTo(new Date()) > 0) {
            return gecikmeZammi;
        }

        gecikmeZammi = gecikmeZamminiHesapla(tutar, vadeTarihi);

        return gecikmeZammi;
    }

    private BigDecimal gecikmeZamminiHesapla(BigDecimal tutar, Date vadeTarihi) {
        BigDecimal gecikmeZammiOrani = gecikmeZammiOraniEntityServis.getGecikmeZammiOrani();

        Date gununTarihi = DateUtil.getGununTarihi();

        final long gunFarkiL = DateUtil.gunFarkiniHesapla(vadeTarihi, gununTarihi);

        BigDecimal gunFarki = BigDecimal.valueOf(gunFarkiL);

        final BigDecimal gecikmeZammi = gecikmeZammiOrani.multiply(gunFarki).multiply(tutar);

        gecikmeZammi.setScale(2, BigDecimal.ROUND_HALF_DOWN);

        if(gecikmeZammi.compareTo(BigDecimal.ZERO) > 0) {
            System.out.println("Geçikme Zammı Hesaplandı Tutar: " + gecikmeZammi);
        }

        return gecikmeZammi;
    }
}
