package main.bilalsoftware.structural.flywight;

public abstract class Asker {

    private MermiBoyutu mermiBoyutu;

    public Asker(MermiBoyutu mermiBoyutu) {
        this.mermiBoyutu = mermiBoyutu;
    }

    public void atesEt() {
        final Mermi mermi = MermiFactory.mermiUret(mermiBoyutu);
        //Mermi mermi = new Mermi(mermiBoyutu);
        mermi.atesle();
    }

    public void tara() {
        for(int i = 0; i < 5 ; i++) {
            atesEt();
        }
    }
}
