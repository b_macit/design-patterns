package main.bilalsoftware.structural.adapter;

public class TelefonElektirikliEvAletiAdapter implements ElektrikliEvAletleri{

    private Telefon telefon;

    public TelefonElektirikliEvAletiAdapter(Telefon telefon) {
        this.telefon = telefon;
    }

    @Override
    public int prizeTakVeCalistir() {
        return telefon.sarjEt();
    }
}
