package main.bilalsoftware.structural.adapter;

public class Buzdolabi implements ElektrikliEvAletleri{
    @Override
    public int prizeTakVeCalistir() {
        System.out.println("Buzdolabi çalışıyor");
        return 210;
    }
}
