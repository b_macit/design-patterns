package main.bilalsoftware.structural.adapter;

public class Test {

    public static void main(String[] args) {
        Priz priz = new Priz();
        Utu utu = new Utu();
        Buzdolabi buzdolabi = new Buzdolabi();

        priz.elektrikVer(utu);
        priz.elektrikVer(buzdolabi);

        IphoneTelefon iphoneTelefon = new IphoneTelefon();
        //hata verir iphone telefon elektrikliEvAletkerinden implement olmuyor
        //priz.elektrikVer(iphoneTelefon);

        //Adaptor telefonla elek. ev al. arasında adaptor koyuyor
        TelefonElektirikliEvAletiAdapter aletiAdapter = new TelefonElektirikliEvAletiAdapter(iphoneTelefon);

        aletiAdapter.prizeTakVeCalistir();
    }
}
