package main.bilalsoftware.structural.proxy;

import java.math.BigDecimal;

public class App {

    public static void main(String[] args) {

        final YoneticiProxy yoneticiProxy = new YoneticiProxy("Yusuf", "123");

        try {
            final BigDecimal ciro = yoneticiProxy.getCiro();
            System.out.println("şirket cirosu " + ciro);
        } catch (IllegalAccessException e) {
            System.out.println("İzniniz yok");
        }

    }
}
