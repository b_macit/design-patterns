package main.bilalsoftware.structural.proxy;

import java.math.BigDecimal;

public class YoneticiProxy implements SirketBilgileri{

    private GercekYonetici gercekYonetici;
    private String kullaniciAdi;
    private String sifre;

    public YoneticiProxy(String kullaniciAdi, String sifre) {
        this.gercekYonetici = new GercekYonetici();
        this.kullaniciAdi = kullaniciAdi;
        this.sifre = sifre;
    }

    private boolean isKullaniciYonetici() {
        boolean isKullaniciYonetici = false;

        boolean isKullaniciValid = CalisanUtil.isKullaniciValid(kullaniciAdi, sifre);

        if(isKullaniciValid) {
            final Calisan calisan = CalisanUtil.getCalisanByKullaniciAdi(kullaniciAdi);

            isKullaniciYonetici = calisan.getYonetici();
        }

        return isKullaniciYonetici;
    }

    @Override
    public BigDecimal getCiro() throws IllegalAccessException {

        final boolean kullaniciYonetici = isKullaniciYonetici();

        if(kullaniciYonetici) {
            return gercekYonetici.getCiro();
        } else {
            throw new IllegalAccessException();
        }
    }
}
