package main.bilalsoftware.structural.proxy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalisanUtil {

    public static List<Calisan> getCalisanList() {
        final Calisan bilal = new Calisan("Bilal", "123", false);
        final Calisan ayse = new Calisan("Ayse", "123", false);
        final Calisan yusuf = new Calisan("Yusuf", "123", true);

        List<Calisan> calisanList = new ArrayList<>();

        calisanList.add(yusuf);
        calisanList.add(bilal);
        calisanList.add(ayse);

        return calisanList;
    }

    public static Map<String, Calisan>  getKullaniciCalisanMap() {
        List<Calisan> calisanList = getCalisanList();

        Map<String, Calisan> calisanMap = new HashMap<>();
        for(Calisan calisan : calisanList) {
            calisanMap.put(calisan.getKullaniciAdi(), calisan);
        }

        return calisanMap;

    }

    public static Calisan getCalisanByKullaniciAdi (String kullaniciAdi) {
        Map<String, Calisan> calisanMap = getKullaniciCalisanMap();

        return calisanMap.get(kullaniciAdi);
    }

    public static boolean isKullaniciValid(String kullaniciAdi, String sifre) {

        boolean isKullaniciValid = false;

        final Calisan calisan = getCalisanByKullaniciAdi(kullaniciAdi);

        if(calisan != null) {
            isKullaniciValid = calisan.getSifre().equals(sifre);
        }

        return isKullaniciValid;
    }
}
