package main.bilalsoftware.structural.composite.son;



import java.math.BigDecimal;

public class App2 {

    public static void main(String[] args) {
        Urun domates = new Urun("Domates", BigDecimal.TEN);
        Urun patates = new Urun("Patates", BigDecimal.ONE);
        Urun sopan = new Urun("Domates", BigDecimal.TEN);

        Paket sebzePaketi = new Paket("Sebze Paketi");
        sebzePaketi.getUrunList().add(sopan);
        sebzePaketi.getUrunList().add(patates);
        sebzePaketi.getUrunList().add(domates);

        Urun sesSistemi = new Urun("Ses sistemi", BigDecimal.valueOf(200));
        Urun kulaklik = new Urun("kulaklık", BigDecimal.valueOf(100));

        Paket teknolojiPaketi = new Paket("Teknoloji Paketi");
        teknolojiPaketi.getUrunList().add(kulaklik);
        teknolojiPaketi.getUrunList().add(sesSistemi);

        Urun futbolTopu = new Urun("Top", BigDecimal.TEN);

        Sepet sepet = new Sepet("sepet");
        sepet.getFiyatlanabilirList().add(teknolojiPaketi);
        sepet.getFiyatlanabilirList().add(sebzePaketi);
        sepet.getFiyatlanabilirList().add(futbolTopu);
        //sepet.getFiyatlanabilirList().add(kumanya);

        final BigDecimal toplamTutar = sepet.toplamOdenecekTutar();
        System.out.println("Toplam tutar " + toplamTutar);
    }
}
