package main.bilalsoftware.structural.composite.son;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Kumanya implements Fiyatlanabilir {

    private String adi;
    private List<Urun> urunList;
    private List<Paket> paketList;

    public Kumanya(String adi) {
        this.adi = adi;
        this.paketList = new ArrayList<>();
        this.urunList = new ArrayList<>();
    }

    public String getAdi() {
        return adi;
    }

    public void setAdi(String adi) {
        this.adi = adi;
    }

    public List<Urun> getUrunList() {
        return urunList;
    }

    public void setUrunList(List<Urun> urunList) {
        this.urunList = urunList;
    }

    public List<Paket> getPaketList() {
        return paketList;
    }

    public void setPaketList(List<Paket> paketList) {
        this.paketList = paketList;
    }

    @Override
    public BigDecimal getFiyat() {
        final BigDecimal bigDecimal = FiyatHesaplamaUtil.toplamUrunFiyati(urunList);
        final BigDecimal bigDecimal1 = FiyatHesaplamaUtil.toplamPaketFiyati(paketList);

        return bigDecimal.add(bigDecimal1);
    }
}
