package main.bilalsoftware.structural.composite.son;

import java.math.BigDecimal;
import java.util.List;

public class FiyatHesaplamaUtil {

    public static BigDecimal toplamUrunFiyati(List<Urun> urunList) {
        BigDecimal toplamtutar = BigDecimal.ZERO;

        for(Urun urun : urunList) {
            toplamtutar = toplamtutar.add(urun.getFiyat());
        }

        return toplamtutar;
    }

    public static BigDecimal toplamPaketFiyati(List<Paket> paketList) {
        BigDecimal toplamtutar = BigDecimal.ZERO;

        for(Paket paket : paketList) {
            toplamtutar = toplamtutar.add(paket.getFiyat());
        }

        return toplamtutar;
    }
}
