package main.bilalsoftware.structural.composite.son;

import java.math.BigDecimal;

public interface Fiyatlanabilir {

    BigDecimal getFiyat();
}
