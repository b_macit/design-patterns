package main.bilalsoftware.creational.prototype;

public class Belge implements Cloneable {

    private Long id;
    private String ad;
    private BelgeTuru belgeTuru;
    private Kategori kategori;
    private String veri;

    public Belge() {
    }

    public Belge(Long id, String ad, BelgeTuru belgeTuru, Kategori kategori, String veri) {
        this.id = id;
        this.ad = ad;
        this.belgeTuru = belgeTuru;
        this.kategori = kategori;
        this.veri = veri;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public BelgeTuru getBelgeTuru() {
        return belgeTuru;
    }

    public void setBelgeTuru(BelgeTuru belgeTuru) {
        this.belgeTuru = belgeTuru;
    }

    public Kategori getKategori() {
        return kategori;
    }

    public void setKategori(Kategori kategori) {
        this.kategori = kategori;
    }

    public String getVeri() {
        return veri;
    }

    public void setVeri(String veri) {
        this.veri = veri;
    }

    @Override
    public String toString() {
        return "Belge{" +
                "id=" + id +
                ", ad='" + ad + '\'' +
                ", belgeTuru=" + belgeTuru +
                ", kategori=" + kategori +
                ", veri='" + veri + '\'' +
                '}';
    }

    //shallow copy yani kendi alt nesnelerinin referanslarını tutar
    /*
    @Override
    protected Belge clone() throws CloneNotSupportedException {
        return (Belge) super.clone();
    }
     */

    // deep copy
    @Override
    protected Belge clone() throws CloneNotSupportedException {
        final Belge clone = (Belge) super.clone();
        final BelgeTuru belgeTuru = (BelgeTuru) clone.getBelgeTuru().clone();
        final Kategori kategori = (Kategori) clone.getKategori().clone();
        clone.setBelgeTuru(belgeTuru);
        clone.setKategori(kategori);
        return clone;
    }

}
