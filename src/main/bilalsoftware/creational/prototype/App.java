package main.bilalsoftware.creational.prototype;

import java.util.Date;

public class App {

    // Amaç; Maliyetli nesneleri oluştururken daha az maaliyetli olan copy'i kullanırız, çeşit shallow ve deep copy vardır.
    // örnekte 4 snde oluşan nesneleri deepcopy ile çok az sürede üretebildik.
    public static void main(String[] args) {

        GenelEntityServis genelEntityServis = new GenelEntityServis();

        Date ilkTarih = new Date();
        final Belge belge1 = genelEntityServis.findBelgeById(1L);
        Date sonTarih = new Date();

        Long fark = getSaniyeFarki(ilkTarih, sonTarih);
        System.out.println(belge1.toString());
        System.out.println(fark);

        Date ilkTarih2 = new Date();
        final Belge belge2 = genelEntityServis.findBelgeById(2L);
        Date sonTarih2 = new Date();

        Long fark2 = getSaniyeFarki(ilkTarih2, sonTarih2);
        System.out.println(belge2.toString());
        System.out.println(fark2);

        //////// ------- ////////

        Belge cloneBelge = null;
        Date ilkTarih3 = new Date();
        try {
            cloneBelge = belge1.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        Date sonTarih3 = new Date();

        Long fark3 = getSaniyeFarki(ilkTarih3, sonTarih3);
        System.out.println(cloneBelge.toString());
        System.out.println(fark3);

    }

    private static Long getSaniyeFarki(Date ilkTarih, Date sonTarih) {
        return (sonTarih.getTime() / 1000) - (ilkTarih.getTime() / 1000);
    }
}
