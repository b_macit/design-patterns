package main.bilalsoftware.creational.abstractFactory;

public class TelefonBayii {

    public static void main(String[] args) {
        Iphone11Factory iphone11Factory = new Iphone11Factory();
        final Telefon iphone11 = iphone11Factory.getTelefon("Iphone11", "4000mah", 4, 8);

        IphoneXFactory iphoneXFactory = new IphoneXFactory();
        final Telefon iphoneX = iphoneXFactory.getTelefon("IphoneX", "3000mah", 3, 7);

        System.out.println("iphone 11 için özellikleri " + iphone11.toString());
        System.out.println("iphone X için özellikleri " + iphoneX.toString());
    }
}
