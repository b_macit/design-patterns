package main.bilalsoftware.creational.abstractFactory;

public class IphoneXFactory implements TelefonFactory{

    @Override
    public Telefon getTelefon(String model, String batarya, int en, int boy) {
        return new IphoneX(model, batarya, en, boy);
    }
}
