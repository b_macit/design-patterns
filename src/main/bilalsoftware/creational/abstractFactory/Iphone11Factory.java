package main.bilalsoftware.creational.abstractFactory;

public class Iphone11Factory implements TelefonFactory{
    @Override
    public Telefon getTelefon(String model, String batarya, int en, int boy) {
        return new Iphone11(model, batarya, en, boy);
    }
}
