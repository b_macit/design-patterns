package main.bilalsoftware.creational.abstractFactory;

public interface TelefonFactory {

    Telefon getTelefon(String model, String batarya, int en, int boy);
}
