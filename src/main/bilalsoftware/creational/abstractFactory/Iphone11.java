package main.bilalsoftware.creational.abstractFactory;

public class Iphone11 implements Telefon {

    String model;
    String batarya;
    int en;
    int boy;

    public Iphone11(String model, String batarya, int en, int boy) {
        this.model = model;
        this.batarya = batarya;
        this.en = en;
        this.boy = boy;
    }

    @Override
    public String getModel() {
        return null;
    }

    @Override
    public String getBatarya() {
        return null;
    }

    @Override
    public int getEn() {
        return 0;
    }

    @Override
    public int getBoy() {
        return 0;
    }

    @Override
    public String toString() {
        return "Iphone11{" +
                "model='" + model + '\'' +
                ", batarya='" + batarya + '\'' +
                ", en=" + en +
                ", boy=" + boy +
                '}';
    }
}
