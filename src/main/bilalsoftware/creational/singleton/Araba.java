package main.bilalsoftware.creational.singleton;

public class Araba {

    //burada arabayı yaratsaydık eager olacaktı ama biz istediğimiz an yaratılmasını istediğimizde getAraba() methodunun içine aldık ve lazy yaptık.
    private static Araba araba;

    private static int sayi = 0;

    private Araba() {
        System.out.println("Ben oluştum");
    }

    public static Araba getAraba() {

        //multi thread programlarda mümkün olduğu kadar az synchronized ile karşılaşması lazım yoksa ciddi anlamda yavaşlama yaşarız.
        //bu yüzden ilk obje yaratılmış mı diye kontrol ediyor yaratılmış ise hiç syncronized ile karşılaşmıyor, yaratılmamışsa sadece bir thread onu yaratıyor.
        //bizim buradaki promlemimiz aynı anda birden fazla thread araba nesnesini ürütmeye çalışması

        //bu tasarım kalıbının adı singleton lazy loading and double checked locking
        if(araba == null) {
            synchronized (Araba.class) {
                if(araba == null) {
                    araba = new Araba();
                }
            }
        }


        sayi++;
        System.out.println(sayi);
        return araba;
    }

}
