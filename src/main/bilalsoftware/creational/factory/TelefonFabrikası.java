package main.bilalsoftware.creational.factory;

public class TelefonFabrikası {

    public static Telefon getTelefon(String model, String batarya, int en, int boy) {

        Telefon telefon;

        //Factory tasarım kalıbının kötü bir özelliği ise budur yüzlerce modeli kontrol edip oluşturmak. Bunun sorunun abstract factory pattern çözüyor.
        if("Iphone11".equalsIgnoreCase(model)) {
            telefon = new Iphone11(model, batarya, en, boy);
        } else if("IphoneX".equalsIgnoreCase(model)) {
            telefon = new IphoneX(model, batarya, en, boy);
        } else {
            throw new RuntimeException("Geçerli bir model değildir");
        }

        return telefon;
    }
}
