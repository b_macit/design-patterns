package main.bilalsoftware.creational.factory;

public class TelefonBayii {

    public static void main(String[] args) {
        final Telefon iphone11 = TelefonFabrikası.getTelefon("Iphone11", "4000mah", 4, 8);
        final Telefon iphoneX = TelefonFabrikası.getTelefon("IphoneX", "3000mah", 3, 7);

        System.out.println("iphone 11 için özellikleri " + iphone11.toString());
        System.out.println("iphone X için özellikleri " + iphoneX.toString());
    }
}
