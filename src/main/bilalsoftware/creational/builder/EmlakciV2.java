package main.bilalsoftware.creational.builder;

public class EmlakciV2 {

    public static void main(String[] args) {
        final Ev ev = EvBuilder.startBuilder()
                .setIl("İstabul")
                .setIlce("Mahalle").build();

        printEv(ev);
    }

    private static void printEv(Ev ev) {
        System.out.println();

        System.out.println("Ev eklendi -> " + ev.toString());

        System.out.println();
    }
}
