package main.bilalsoftware.creational.builder;

public class Emlakci {

    public static void main(String[] args) {
        Ev ev1 = new Ev();
        ev1.setIl("Istanbul");
        ev1.setIlce("Maltepe");
        ev1.setMahalle("Girne");
        ev1.setOdaSayisi(3);
        ev1.setBinaYasi(10);
        ev1.setHasKlima(true);

        Ev ev2 = new Ev("Istanbul", "Ataşehir", "A", 10, 3,2,2,1,false,false,true,true,true,true);

        printEv(ev1);
        printEv(ev2);
    }

    private static void printEv(Ev ev) {
        System.out.println();

        System.out.println("Ev eklendi -> " + ev.toString());

        System.out.println();
    }
}
