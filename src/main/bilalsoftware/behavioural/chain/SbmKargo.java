package main.bilalsoftware.behavioural.chain;

public class SbmKargo {

    public static KargoSirketi getKargoSirketi() {

        final CankiriSube cankiriSube = new CankiriSube();
        final AnkaraSube ankaraSube = new AnkaraSube();
        final IstanbulSube istanbulSube = new IstanbulSube();
        final AntalyaSube antalyaSube = new AntalyaSube();

        final KargoSirketi sbmKargo = cankiriSube
                .setSonrakiKargoSirketi(ankaraSube
                        .setSonrakiKargoSirketi(istanbulSube
                                .setSonrakiKargoSirketi(antalyaSube)));

        return sbmKargo;
    }
}
