package main.bilalsoftware.behavioural.chain;

public abstract class KargoSirketi {

    private Il il;
    private KargoSirketi sonrakiKargoSirketi;

    public KargoSirketi(Il il) {
        this.il = il;
    }

    public Il getIl() {
        return il;
    }

    public KargoSirketi getSonrakiKargoSirketi() {
        return sonrakiKargoSirketi;
    }

    public KargoSirketi setSonrakiKargoSirketi(KargoSirketi sonrakiKargoSirketi) {
        this.sonrakiKargoSirketi = sonrakiKargoSirketi;
        return this;
    }

    public void kargola(Il il) {
        gelenKargoBilgileriYazdir(il);
        if(getIl().equals(il)) {
            kargoTeslimBilgileriYazdir();
        }else if (getSonrakiKargoSirketi() != null) {
            getSonrakiKargoSirketi().kargola(il);
        }else {
            hizmetAlanDisiYazdir(il);
        }
    }

    private void hizmetAlanDisiYazdir(Il il) {
        System.out.println(il.getIl() + " hizmet alanının dışındadır.");
    }

    private void kargoTeslimBilgileriYazdir() {
        System.out.println(getIl().getIl() + " şube teslim aldı.");
    }

    private void gelenKargoBilgileriYazdir(Il il) {
        System.out.println(il.getIl() + " iline gidecek kargo "+ getIl().getIl() + " subesine geldi.");
    }
}
