package main.bilalsoftware.behavioural.chain;

public class App {

    public static void main(String[] args) {

        final KargoSirketi sbmKargo = SbmKargo.getKargoSirketi();

        sbmKargo.kargola(Il.KARS);

        System.out.println("\n --------------- \n");

        sbmKargo.kargola(Il.ANTALYA);

        System.out.println("\n --------------- \n");

        sbmKargo.kargola(Il.CANKIRI);
        System.out.println("\n --------------- \n");

        sbmKargo.kargola(Il.ISTANBUL);
        System.out.println("\n --------------- \n");
    }
}
