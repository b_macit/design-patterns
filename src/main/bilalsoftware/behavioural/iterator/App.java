package main.bilalsoftware.behavioural.iterator;

import java.util.List;

public class App {

    public static void main(String[] args) {

//        Kitap kitap = new Kitap("Matematik");
//        final List<Soru> soruList = kitap.getSoruList();
//
//        yazdir(soruList);
//
//        Fasikul fasikul = new Fasikul("Turkce");
//        final Soru[] sorular = fasikul.getSorular();
//
//        yazdir(sorular);
    }

    private static void yazdir(Soru[] sorular) {
        for(Soru soru : sorular) {
            final Long soruNumarasi = soru.getSoruNumarasi();

            System.out.println("Soru no: " + soruNumarasi);
        }
    }

    private static void yazdir(List<Soru> soruList) {
        for(Soru soru : soruList) {
            final Long soruNumarasi = soru.getSoruNumarasi();

            System.out.println("Soru no: " + soruNumarasi);
        }
    }
}
