package main.bilalsoftware.behavioural.iterator;

import java.util.Iterator;

public class App2 {

    public static void main(String[] args) {

        Kitap kitap = new Kitap("Matematik");
        final Iterator soruIterator = kitap.getSoruIterator();

        yazdir(soruIterator);

        Fasikul fasikul = new Fasikul("Turkce");
        final Iterator soruArrayIterator = fasikul.getSoruIterator();

        yazdir(soruArrayIterator);
    }

    private static void yazdir(Iterator iterator) {

        while(iterator.hasNext()) {
            final Soru soru = (Soru) iterator.next();
            final Long soruNumarasi = soru.getSoruNumarasi();

            System.out.println("Soru no: " + soruNumarasi);
        }
    }
}
