package main.bilalsoftware.behavioural.iterator;

import java.util.Iterator;

public class Fasikul {

    private String adi;
    private Soru[] sorular;

    public Fasikul(String adi) {
        this.adi = adi;
        this.sorular = new Soru[10];
        sorular[0] = new Soru(6l);
        sorular[1] = new Soru(7l);
        sorular[2] = new Soru(8l);
        sorular[3] = new Soru(9l);
        sorular[4] = new Soru(10l);
        sorular[5] = new Soru(11l);
        sorular[6] = new Soru(12l);
        sorular[7] = new Soru(13l);
        sorular[8] = new Soru(14l);
        sorular[9] = new Soru(15l);
    }

    public String getAdi() {
        return adi;
    }

    public void setAdi(String adi) {
        this.adi = adi;
    }

    public Iterator getSoruIterator() {
        return new SoruIterator(sorular);
    }

}
