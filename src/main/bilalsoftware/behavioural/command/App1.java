package main.bilalsoftware.behavioural.command;

import java.math.BigDecimal;

public class App1 {

    public static void main(String[] args) {
        final BigDecimal islem = HesapMakinasi1.islem(IslemTuru.TOPLAMA, BigDecimal.TEN, BigDecimal.ONE);

        System.out.println("Sonuc " + islem);
    }
}
