package main.bilalsoftware.behavioural.command;

import java.math.BigDecimal;

public class App2 {

    public static void main(String[] args) {
        final BigDecimal sayi1 = BigDecimal.TEN;
        final BigDecimal sayi2 = BigDecimal.TEN;

        final BigDecimal toplam = HesapMakinasi2.islem(new ToplamaCommand(), sayi1, sayi2);
        final BigDecimal cikarma = HesapMakinasi2.islem(new CikarmaCommand(), sayi1, sayi2);
        final BigDecimal us = HesapMakinasi2.islem(new UsCommand(), sayi1, sayi2);
        final BigDecimal bolme = HesapMakinasi2.islem(new BolmeCommand(), sayi1, sayi2);
        final BigDecimal mod = HesapMakinasi2.islem(new ModCommand(), sayi1, sayi2);

        System.out.println("toplam " + toplam);
        System.out.println("fark " + cikarma);
        System.out.println("us " + us);
        System.out.println("bolme " + bolme);
        System.out.println("mod " + mod);
    }
}
