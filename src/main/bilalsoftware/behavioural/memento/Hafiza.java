package main.bilalsoftware.behavioural.memento;

import java.util.ArrayList;
import java.util.List;

public class Hafiza {

    private List<NotePadMemento> list;

    public Hafiza() {
        this.list = new ArrayList<>();
    }

    public void ekle(NotePadMemento memento) {
        list.add(memento);
    }

    public NotePadMemento sonVersionuGetir() {
        if(!list.isEmpty()) {
            final NotePadMemento sonVersion = list.get(list.size() - 1);
            list.remove(sonVersion);

            return sonVersion;
        } else {
            throw new ArrayIndexOutOfBoundsException("Yuklu version bulunamadı.");
        }
    }
}
