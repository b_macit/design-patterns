package main.bilalsoftware.behavioural.memento;

public class App {

    public static void main(String[] args) {
        Hafiza hafiza = new Hafiza();

        NotePad notePad = new NotePad();

        notePad.ekle("B");
        notePad.ekle("İ");
        notePad.ekle("L");

        versionKaydet(hafiza, notePad);

        notePad.ekle("AL");

        versionKaydet(hafiza, notePad);

        hataliNoktaKoy(notePad);

        geriAl(hafiza, notePad);

        versionKaydet(hafiza, notePad);

        notePad.ekle(" Macit");

        versionKaydet(hafiza, notePad);

        hataliNoktaKoy(notePad);

        geriAl(hafiza, notePad);
        geriAl(hafiza, notePad);
        geriAl(hafiza, notePad);

    }

    private static void geriAl(Hafiza hafiza, NotePad notePad) {
        notePad.geriDon(hafiza.sonVersionuGetir());

        notePad.yazdir();
    }

    private static void hataliNoktaKoy(NotePad notePad) {
        notePad.ekle(".");

        notePad.yazdir();
    }

    private static void versionKaydet(Hafiza hafiza, NotePad notePad) {
        NotePadMemento bil = notePad.kaydet();
        hafiza.ekle(bil);
        notePad.yazdir();
    }


}
