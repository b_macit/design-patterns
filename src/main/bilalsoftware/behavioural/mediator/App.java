package main.bilalsoftware.behavioural.mediator;

import java.math.BigDecimal;

public class App {

    public static void main(String[] args) {
        Kabzimal kabzimal = new Kabzimal();

        final DomatesUreticisi bilal = new DomatesUreticisi("Bilal", BigDecimal.valueOf(4L), kabzimal);
        final DomatesUreticisi taha = new DomatesUreticisi("Taha", BigDecimal.valueOf(3L), kabzimal);

        kabzimal.ureticiEkle(bilal);
        kabzimal.ureticiEkle(taha);

        final DomatesHalcisi ahmet = new DomatesHalcisi("Ahmet", BigDecimal.valueOf(5L), kabzimal);
        final DomatesHalcisi mehmet = new DomatesHalcisi("Mehmet", BigDecimal.valueOf(7L), kabzimal);

        kabzimal.halciEkle(ahmet);
        kabzimal.halciEkle(mehmet);

        bilal.urunSat();
        mehmet.urunAl();
    }
}
