package main.bilalsoftware.behavioural.interpreter;

import main.bilalsoftware.behavioural.interpreter.yorumlayici.KargaDiliYorumlayici;
import main.bilalsoftware.behavioural.interpreter.yorumlayici.KusDiliYorumlayici;

public class App {

    public static void main(String[] args) {

        String kusSozu = "Sacadıcık Bacahacadıcır Mecemiciş cacacıcık yecer";

        KusDiliYorumlayici kusDiliYorumlayici = new KusDiliYorumlayici();
        final String yorumla = kusDiliYorumlayici.yorumla(kusSozu);

        System.out.println(yorumla);

        String kargaSozu = "Sagadıgık Bagahagadıgır Megemigiş cagacıgık yeger";

        KargaDiliYorumlayici kargaDiliYorumlayici = new KargaDiliYorumlayici();
        final String kargaDili = kargaDiliYorumlayici.yorumla(kargaSozu);

        System.out.println(kargaDili);

    }
}
