package main.bilalsoftware.behavioural.interpreter.yorumlayici;

import main.bilalsoftware.behavioural.interpreter.dil.KargaDil;

public class KargaDiliYorumlayici implements Yorumlayici{
    @Override
    public String yorumla(String soz) {
        KargaDil kargaDil = new KargaDil();
        return kargaDil.turkceyeCevir(soz);
    }
}
