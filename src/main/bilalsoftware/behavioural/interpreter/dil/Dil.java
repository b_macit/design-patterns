package main.bilalsoftware.behavioural.interpreter.dil;

public abstract class Dil {

    private EnumDil enumDil;

    public Dil(EnumDil enumDil) {
        this.enumDil = enumDil;
    }

    public String turkceyeCevir(String soz) {
        String[] kelimeler = soz.split(" ");
        String turkce = "";

        for(String kelime : kelimeler) {

            String turkceKelime = kelimeyiTurkceyeCevir(kelime);

            turkce = turkce + " " + turkceKelime;
        }
        return turkce;
    }

    /**
     * Secelacam -> se-ce-la-ca-m
     * @param kelime
     * @return
     */
    private String kelimeyiTurkceyeCevir(String kelime) {
        String kelimeAyarlayici = kelime;

        String heceleri = helece(kelimeAyarlayici);

        final String[] heceList = heceleri.split("-");

        String kelimeninTurkcesi = hecelerdenKelimeBul(heceList);

        return kelimeninTurkcesi;
    }

    private String helece(String kelimeAyarlayici) {
        String hecelenmisHali = "";
        while (kelimeAyarlayici.length() > 0) {
            String hece = getHece(kelimeAyarlayici);
            hecelenmisHali = hecelenmisHali + hece + "-";
            kelimeAyarlayici = kelimeAyarlayici.replaceFirst(hece, "");
        }
        return hecelenmisHali;
    }

    private String getHece(String kelime) {
        int boyut = enumDil.getEk().length();
        if(kelime.length() < enumDil.getEk().length()) {
            boyut = kelime.length();
        }

        final String substring = kelime.substring(0, boyut);
        return substring;
    }

    private String hecelerdenKelimeBul(String[] heceList) {
        int i = 0;

        String turkceKelime = "";

        for(String hece : heceList) {
            if(i % 2 == 0) {
                turkceKelime = turkceKelime + hece;
            }
            i++;
        }
        return turkceKelime;
    }
}
