package main.bilalsoftware.behavioural.observer;

public class Ebebeyn implements Observer {

    private String adi;

    public Ebebeyn(String adi) {
        this.adi = adi;
    }

    @Override
    public void update(Observable observable) {
        Termometre termometre = (Termometre) observable;
        System.out.println(adi + " dedi ki: oo sicaklık " + termometre.getAnlikSicaklik() + " derece olmus");
    }
}
