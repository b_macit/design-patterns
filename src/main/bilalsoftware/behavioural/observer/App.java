package main.bilalsoftware.behavioural.observer;

import java.math.BigDecimal;

public class App {

    public static void main(String[] args) {
        final BigDecimal minSicaklik = BigDecimal.valueOf(23);
        final BigDecimal maxSicaklik = BigDecimal.valueOf(28);

        final Termometre termometre = new Termometre(minSicaklik, maxSicaklik);

        final Ebebeyn anne = new Ebebeyn("Anne");
        final Ebebeyn baba = new Ebebeyn("Baba");

        termometre.ekle(anne);
        termometre.ekle(baba);

        for(int i = termometre.getAnlikSicaklik().intValue(); i <= 30; i++) {
            termometre.setAnlikSicaklik(BigDecimal.valueOf(i));
        }

        for(int i = termometre.getAnlikSicaklik().intValue(); i >= 22; i--) {
            termometre.setAnlikSicaklik(BigDecimal.valueOf(i));
        }
    }
}
