package main.bilalsoftware.behavioural.observer;

public interface Observer {

    void update(Observable observable);
}
